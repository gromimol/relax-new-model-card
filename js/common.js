$(document).ready(function () {
    $('.favorite').on('click', function () {
        $(this).toggleClass('active')
    })

    // model-card__main__slider
    $('.model-card__main__slider').slick({
        slidesToShow: 1,
        dots: true,
        prevArrow: '<span class="prev-arrow"><svg width="7" height="11" viewBox="0 0 7 11" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M5.41162 1.44122L0.844558 5.8962L5.41243 10.3504" stroke="currentColor" stroke-width="1.2" stroke-linecap="round" stroke-linejoin="round"/></svg></span>',
        nextArrow: '<span class="next-arrow"><svg width="7" height="11" viewBox="0 0 7 11" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1.58838 1.44122L6.15544 5.8962L1.58757 10.3504" stroke="currentColor" stroke-width="1.2" stroke-linecap="round" stroke-linejoin="round"/></svg></span>'
    })
    // show-more-info
    $('.show-more-info').on('click', function () {
        $(this).toggleClass('active');
        $(this).closest('.model-card').toggleClass('show')
        if($(this).closest('.model-card').hasClass('show')) {
            $(this).text('Показать меньше')
        } else {
            $(this).text('Показать больше')
        }
    })
    // Скрыть\показать часть номера телефона
    $(function(){
        let holder=$('.phone_number'),
            holderAttr=holder.attr('href', 'tel:'),
            button=$('.phone_number_active'),
            number=holder.text(),
            symbolsForHide=5,
            show=()=>{
                holder.text(number)
                button.removeClass('show').text('Скрыть')
                holderAttr=holder.attr('href', `tel:${number}`)
            },
            hide=()=>{
                holder.text(number.replace(new RegExp('(.+).{'+symbolsForHide+'}$'),"$1"+'.'.repeat(symbolsForHide)))
                button.addClass('show').text('Показать')
                holderAttr=holder.attr('href', 'tel:')
            }
        button.click(function(){
            if($(this).hasClass('show')) show()
            else hide()
        })
        hide()
    })
    // show popup
    $('.show-detail').on('click', function (e) {
        e.preventDefault()
        $('body').addClass('show-quick-view')
    })
    $('#overlay, .popup-model-detail .close').on('click', function (e) {
        e.preventDefault()
        $('body').removeClass('show-quick-view')
    })

})
